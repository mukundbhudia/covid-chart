const tsData = [
  {
    confirmed: 553,
    active: 508,
    deaths: 17,
    recovered: 28,
    day: '1/22/20'
  },
  {
    confirmed: 650,
    active: 602,
    deaths: 18,
    recovered: 30,
    day: '1/23/20'
  },
  {
    confirmed: 936,
    active: 874,
    deaths: 26,
    recovered: 36,
    day: '1/24/20'
  },
  {
    confirmed: 1427,
    active: 1346,
    deaths: 42,
    recovered: 39,
    day: '1/25/20'
  },
  {
    confirmed: 2110,
    active: 2004,
    deaths: 56,
    recovered: 50,
    day: '1/26/20'
  },
  {
    confirmed: 2919,
    active: 2778,
    deaths: 82,
    recovered: 59,
    day: '1/27/20'
  },
  {
    confirmed: 5564,
    active: 5331,
    deaths: 131,
    recovered: 102,
    day: '1/28/20'
  },
  {
    confirmed: 6152,
    active: 5898,
    deaths: 133,
    recovered: 121,
    day: '1/29/20'
  },
  {
    confirmed: 8220,
    active: 7911,
    deaths: 171,
    recovered: 138,
    day: '1/30/20'
  },
  {
    confirmed: 9908,
    active: 9478,
    deaths: 213,
    recovered: 217,
    day: '1/31/20'
  },
  {
    confirmed: 12019,
    active: 11481,
    deaths: 259,
    recovered: 279,
    day: '2/1/20'
  },
  {
    confirmed: 16768,
    active: 15939,
    deaths: 362,
    recovered: 467,
    day: '2/2/20'
  },
  {
    confirmed: 19862,
    active: 18818,
    deaths: 426,
    recovered: 618,
    day: '2/3/20'
  },
  {
    confirmed: 23867,
    active: 22528,
    deaths: 492,
    recovered: 847,
    day: '2/4/20'
  },
  {
    confirmed: 27610,
    active: 25927,
    deaths: 564,
    recovered: 1119,
    day: '2/5/20'
  },
  {
    confirmed: 30792,
    active: 28676,
    deaths: 634,
    recovered: 1482,
    day: '2/6/20'
  },
  {
    confirmed: 34366,
    active: 31641,
    deaths: 719,
    recovered: 2006,
    day: '2/7/20'
  },
  {
    confirmed: 37088,
    active: 33676,
    deaths: 806,
    recovered: 2606,
    day: '2/8/20'
  },
  {
    confirmed: 40118,
    active: 35978,
    deaths: 906,
    recovered: 3234,
    day: '2/9/20'
  },
  {
    confirmed: 42730,
    active: 37781,
    deaths: 1013,
    recovered: 3936,
    day: '2/10/20'
  },
  {
    confirmed: 44769,
    active: 38983,
    deaths: 1113,
    recovered: 4673,
    day: '2/11/20'
  },
  {
    confirmed: 45188,
    active: 38930,
    deaths: 1118,
    recovered: 5140,
    day: '2/12/20'
  },
  {
    confirmed: 60335,
    active: 52681,
    deaths: 1371,
    recovered: 6283,
    day: '2/13/20'
  },
  {
    confirmed: 66852,
    active: 57283,
    deaths: 1523,
    recovered: 8046,
    day: '2/14/20'
  },
  {
    confirmed: 68997,
    active: 57948,
    deaths: 1666,
    recovered: 9383,
    day: '2/15/20'
  },
  {
    confirmed: 71190,
    active: 58569,
    deaths: 1770,
    recovered: 10851,
    day: '2/16/20'
  },
  {
    confirmed: 73223,
    active: 58787,
    deaths: 1868,
    recovered: 12568,
    day: '2/17/20'
  },
  {
    confirmed: 75101,
    active: 58757,
    deaths: 2007,
    recovered: 14337,
    day: '2/18/20'
  },
  {
    confirmed: 75604,
    active: 57376,
    deaths: 2122,
    recovered: 16106,
    day: '2/19/20'
  },
  {
    confirmed: 76162,
    active: 55753,
    deaths: 2247,
    recovered: 18162,
    day: '2/20/20'
  },
  {
    confirmed: 76788,
    active: 55664,
    deaths: 2251,
    recovered: 18873,
    day: '2/21/20'
  },
  {
    confirmed: 78544,
    active: 53217,
    deaths: 2458,
    recovered: 22869,
    day: '2/22/20'
  },
  {
    confirmed: 78930,
    active: 53088,
    deaths: 2469,
    recovered: 23373,
    day: '2/23/20'
  },
  {
    confirmed: 79533,
    active: 51698,
    deaths: 2629,
    recovered: 25206,
    day: '2/24/20'
  },
  {
    confirmed: 80376,
    active: 49785,
    deaths: 2708,
    recovered: 27883,
    day: '2/25/20'
  },
  {
    confirmed: 81355,
    active: 48223,
    deaths: 2770,
    recovered: 30362,
    day: '2/26/20'
  },
  {
    confirmed: 82714,
    active: 46645,
    deaths: 2814,
    recovered: 33255,
    day: '2/27/20'
  },
  {
    confirmed: 84079,
    active: 44524,
    deaths: 2872,
    recovered: 36683,
    day: '2/28/20'
  },
  {
    confirmed: 85969,
    active: 43274,
    deaths: 2941,
    recovered: 39754,
    day: '2/29/20'
  },
  {
    confirmed: 88327,
    active: 42644,
    deaths: 2995,
    recovered: 42688,
    day: '3/1/20'
  },
  {
    confirmed: 90263,
    active: 41608,
    deaths: 3084,
    recovered: 45571,
    day: '3/2/20'
  },
  {
    confirmed: 92797,
    active: 41441,
    deaths: 3159,
    recovered: 48197,
    day: '3/3/20'
  },
  {
    confirmed: 95077,
    active: 40685,
    deaths: 3253,
    recovered: 51139,
    day: '3/4/20'
  },
  {
    confirmed: 97835,
    active: 40723,
    deaths: 3347,
    recovered: 53765,
    day: '3/5/20'
  },
  {
    confirmed: 101736,
    active: 42443,
    deaths: 3459,
    recovered: 55834,
    day: '3/6/20'
  },
  {
    confirmed: 105771,
    active: 43887,
    deaths: 3557,
    recovered: 58327,
    day: '3/7/20'
  },
  {
    confirmed: 109745,
    active: 45281,
    deaths: 3801,
    recovered: 60663,
    day: '3/8/20'
  },
  {
    confirmed: 113511,
    active: 47061,
    deaths: 3987,
    recovered: 62463,
    day: '3/9/20'
  },
  {
    confirmed: 118539,
    active: 49907,
    deaths: 4261,
    recovered: 64371,
    day: '3/10/20'
  },
  {
    confirmed: 125806,
    active: 54223,
    deaths: 4614,
    recovered: 66969,
    day: '3/11/20'
  },
  {
    confirmed: 128273,
    active: 55264,
    deaths: 4719,
    recovered: 68290,
    day: '3/12/20'
  },
  {
    confirmed: 145118,
    active: 69499,
    deaths: 5403,
    recovered: 70216,
    day: '3/13/20'
  },
  {
    confirmed: 156012,
    active: 77605,
    deaths: 5818,
    recovered: 72589,
    day: '3/14/20'
  },
  {
    confirmed: 167332,
    active: 84894,
    deaths: 6439,
    recovered: 75999,
    day: '3/15/20'
  },
  {
    confirmed: 181380,
    active: 96202,
    deaths: 7125,
    recovered: 78053,
    day: '3/16/20'
  },
  {
    confirmed: 196965,
    active: 108262,
    deaths: 7904,
    recovered: 80799,
    day: '3/17/20'
  },
  {
    confirmed: 214698,
    active: 122801,
    deaths: 8732,
    recovered: 83165,
    day: '3/18/20'
  },
  {
    confirmed: 242436,
    active: 147758,
    deaths: 9866,
    recovered: 84812,
    day: "3/19/20"
  },
]

const chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
}

const allDates = []
const confirmed = []
const recovered = []
const deaths = []
const active = []

tsData.forEach(element => {
  const dateFromString = new Date(element.day)
  allDates.push((dateFromString).toLocaleDateString())
  confirmed.push({x: dateFromString, y: element.confirmed})
  recovered.push({x: dateFromString, y: element.recovered})
  deaths.push({x: dateFromString, y: element.deaths})
  active.push({x: dateFromString, y: element.active})
})

let config = {
  type: 'line',
  data: {
    labels: allDates,
    datasets: [
      {
      label: 'Confirmed',
      backgroundColor: chartColors.red,
      borderColor: chartColors.red,
      data: confirmed,
      fill: false,
    }, 
    {
      label: 'Active',
      fill: false,
      backgroundColor: chartColors.blue,
      borderColor: chartColors.blue,
      data: active,
    }, 
    {
      label: 'Recovered',
      fill: false,
      backgroundColor: chartColors.green,
      borderColor: chartColors.green,
      data: recovered,
    }, 
    {
      label: 'Deaths',
      fill: false,
      backgroundColor: chartColors.grey,
      borderColor: chartColors.grey,
      data: deaths,
    }
  ]
  },
  options: {
    responsive: true,
    title: {
      display: true,
      text: 'Covid-19 cases by day'
    },
    tooltips: {
      mode: 'index',
      intersect: false,
    },
    hover: {
      mode: 'nearest',
      intersect: true
    },
    scales: {
      xAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'Date'
        }
      }],
      yAxes: [{
        display: true,
        type: 'linear',
        scaleLabel: {
          display: true,
          labelString: 'Number of cases'
        },
        ticks: {
          beginAtZero: true,
          callback: function(value, index, values) {
              return value.toLocaleString()
          }
        }
      }]
    }
  }
}

const toggleCurrentCases = (showToday, currentCases) => {
  if (showToday) {
    allDates.push((currentCases.date).toLocaleDateString())
    confirmed.push({x: currentCases.date, y: currentCases.confirmed})
    recovered.push({x: currentCases.date, y: currentCases.recovered})
    deaths.push({x: currentCases.date, y: currentCases.deaths})
    active.push({x: currentCases.date, y: currentCases.active})
  } else {
    allDates.pop()
    recovered.pop()
    deaths.pop()
    active.pop()
  }
}

let myChart
let addedFromAPI = false
let showToday = false
const today = new Date()
const currentCases = {}
let lastUpdated

window.onload = function() {
  const Http = new XMLHttpRequest()
  const url='https://covid19.mathdro.id/api'
  Http.open("GET", url)
  Http.send()
  
  Http.onreadystatechange = (e) => {
    if (addedFromAPI === false) {
      let out = JSON.parse(Http.responseText)
      lastUpdated = new Date(out.lastUpdate)
      currentCases.date = today
      currentCases.confirmed = out.confirmed.value
      currentCases.active = (out.confirmed.value - (out.recovered.value + out.deaths.value))
      currentCases.recovered = out.recovered.value
      currentCases.deaths = out.deaths.value

      let confirmedCounter = window.document.getElementById('confirmedCounter')
      let recoveredCounter = window.document.getElementById('recoveredCounter')
      let deathsCounter = window.document.getElementById('deathsCounter')
      let activeCounter = window.document.getElementById('activeCounter')
      let lastUpdatedLabel = window.document.getElementById('lastUpdated')
      lastUpdatedLabel.textContent = lastUpdated.toLocaleString()
      confirmedCounter.textContent = currentCases.confirmed.toLocaleString()
      recoveredCounter.textContent = currentCases.recovered.toLocaleString()
      deathsCounter.textContent = currentCases.deaths.toLocaleString()
      activeCounter.textContent = currentCases.active.toLocaleString()

      addedFromAPI = true

      let ctx = document.getElementById('canvas').getContext('2d')
      myChart = new Chart(ctx, config)
    }
  }

  let changeScaleBtn = window.document.getElementById('changeScale')
  changeScaleBtn.addEventListener('click', function() {
    if (config.data.datasets.length > 0) {
      if (config.options.scales.yAxes[0].type === 'linear') {
        config.options.scales.yAxes[0].type = 'logarithmic'
        changeScaleBtn.textContent = 'View linear'
      } else {
        config.options.scales.yAxes[0].type = 'linear'
        changeScaleBtn.textContent = 'View logarithmic'
      }
      myChart.update()
    }
  })

  let changeChartTypeBtn = window.document.getElementById('changeChartType')
  changeChartTypeBtn.addEventListener('click', function() {
    if (config.data.datasets.length > 0) {
      if (config.type === 'line') {
        config.type = 'bar'
        changeChartTypeBtn.textContent = 'View line chart'
      } else {
        config.type = 'line'
        changeChartTypeBtn.textContent = 'View bar chart'
      }
      myChart.update()
    }
  })

  let toggleCurrentCasesBtn = window.document.getElementById('toggleCurrentCases')
  toggleCurrentCasesBtn.addEventListener('click', function() {
    if (config.data.datasets.length > 0) {
      if (showToday) {
        showToday = false
        toggleCurrentCases(showToday, currentCases)
        toggleCurrentCasesBtn.textContent = 'Add current cases'
      } else {
        showToday = true
        toggleCurrentCases(showToday, currentCases)
        toggleCurrentCasesBtn.textContent = 'Remove current cases'
      }
      myChart.update()
    }
  })
}
